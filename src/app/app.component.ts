import { Component } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { authCodeFlowConfig } from './service/oauth-config.service';
import { JwksValidationHandler } from 'angular-oauth2-oidc-jwks';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(private oauthService: OAuthService){
    this.configureOAuth()
  }

  configureOAuth(){
    this.oauthService.configure(authCodeFlowConfig)
    this.oauthService.tokenValidationHandler=new JwksValidationHandler
    this.oauthService.loadDiscoveryDocumentAndTryLogin()

  }

  login() {
    this.oauthService.initImplicitFlow()
    
  }
  logout() {
    this.oauthService.logOut()
  }
  title = 'oidcoauth';

  get token(){
    let claims:any = this.oauthService.getIdentityClaims();
    return claims? claims : null;
  }
}
