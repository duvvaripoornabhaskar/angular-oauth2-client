import { Component } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { JwksValidationHandler } from 'angular-oauth2-oidc-jwks';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {

  constructor(private oauthService: OAuthService){
    console.log(oauthService.getAccessToken());  
  }

  getToken(){   
    console.log(this.oauthService.getAccessToken());  
    console.log(this.oauthService.getIdToken());
    console.log(this.oauthService.getRefreshToken());  
  }

}
